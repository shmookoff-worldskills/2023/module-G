<?php
/*
 * Plugin Name:       Random Line
 * Plugin URI:        https://gitlab.com/shmookoff-worldskills/2023/module-G
 * Description:       Random line from a post on pages.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            shmookoff
 * Author URI:        https://gitlab.com/shmookoff
 * Text Domain:       random-line
 * Domain Path:       /languages
 */


function random_line_settings_init()
{
    register_setting('reading', 'random_line_post_id');
    add_settings_section('random_line_settings_section', 'Random Line', 'random_line_settings_section_callback', 'reading');
    add_settings_field('random_line_settings_post_id_field', 'Post ID', 'random_line_settings_post_id_callback', 'reading', 'random_line_settings_section');
}

add_action('admin_init', 'random_line_settings_init');


function random_line_settings_section_callback()
{
    echo '';
}

function random_line_settings_post_id_callback()
{
    $post_id = get_option('random_line_post_id');
?>
    <input type="text" name="random_line_post_id" value="<?php echo $post_id; ?>" />
    <?php
}

function post_lines_filter($line)
{
    if ((str_starts_with($line, '<!-- ') && str_ends_with($line, ' -->')) || $line == '') {
        return false;
    }
    return true;
}

function random_line_display()
{
    $post_id = get_option('random_line_post_id');
    $line = '';

    if ($post_id) {
        $post = get_post($post_id);
        if ($post && $post->post_type == 'post' && $post->post_status == 'publish') {
            $lines = array_filter(explode("\n", $post->post_content), 'post_lines_filter');
            if ($lines) {
                $line = trim($lines[array_rand($lines)]);
            }
        }
    }

    if ($line) {
    ?>
        <div id="random-line" style="visibility: hidden;">
            <p class="random-line__heading">Random Line:</p>
            <div class="random-line__content"><?php echo $line ?></div>
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', () => {
                const randomLine = document.getElementById('random-line');
                const offset = randomLine.offsetTop - window.innerHeight;
                const bottom = randomLine.offsetHeight + randomLine.offsetTop;

                window.addEventListener('scroll', () => {
                    if (window.scrollY > offset && window.scrollY < bottom) {
                        randomLine.style.visibility = 'visible';
                        randomLine.style.display = 'block';
                        randomLine.style.animation = 'fade-in 1s forwards';
                    }
                })
            })
        </script>

        <style>
            @keyframes fade-in {
                from {
                    opacity: 0;
                    transform: translateY(-20px);
                }

                to {
                    opacity: 1;
                    transform: translateY(0);
                }
            }

            #random-line {
                position: fixed;
                right: 5%;
                top: 15%;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2);
                border-radius: 5px;
                transition: opacity 0.5s;
                z-index: 9999;
                font-size: 16px;
                color: #333;
                max-width: 20%;
            }

            .random-line__heading {
                font-weight: bold;

            }

            .random-line__content {
                text-align: center;

            }

            #random-line p {
                margin: 0;
            }
        </style>
<?php
    }
}
add_action('wp_footer', 'random_line_display');
