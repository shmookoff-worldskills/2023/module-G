<?php
/*
 * Plugin Name:       QRPage
 * Plugin URI:        https://gitlab.com/shmookoff-worldskills/2023/module-G
 * Description:       Shortcode with qr for page
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            shmookoff
 * Author URI:        https://gitlab.com/shmookoff
 * Text Domain:       qrpage
 * Domain Path:       /languages
 */


//[qrpage]
function qrpage_shortcode($atts)
{

    return '<div class="qrcode"></div>';
}
add_shortcode('qrpage', 'qrpage_shortcode');

function qrpage_enqueue_script()
{
    wp_enqueue_script('qrpage', plugin_dir_url(__FILE__) . 'qrcode.min.js');
}
add_action('wp_enqueue_scripts', 'qrpage_enqueue_script');

function qrpage_render()
{
    global $wp;
    $link = add_query_arg($wp->query_vars, home_url($wp->request));
?>
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            const qrs = document.getElementsByClassName('qrcode');
            console.log(qrs)
            for (const qr of qrs) {
                new QRCode(qr, '<?php echo $link; ?>')
            }
        })
    </script>
<?php
}
add_action('wp_footer', 'qrpage_render');
